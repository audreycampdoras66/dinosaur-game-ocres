# Le jeu du T-Rex

<<<<<<< HEAD
## Etape 5

Vous voilà maintenant avec un début de projet sur `master`, vous pouvez dès à présent le tester sur Heroku.
Pour cela vous devez d'abord vous créer un compte sur heroku et suivre la démarche ci-dessous :

1. Créer son compte sur https://id.heroku.com/login
2. Créer une nouvelle app nommée `dinosaur-game-<votre_nom_ou_surnom>`

![créer une app](assets/step-5-1.png)

![nommer son app](assets/step-5-2.png)

3. Aller dans Settings en copier l'url du répo git

![aller dans settings](assets/step-5-3.png)

4. Configurer le buildpack `mars/create-react-app`

![ajout du buildpack](assets/step-5-4.png)

5. Installer la CLI Heroku https://devcenter.heroku.com/articles/heroku-cli


Nous allons maintenant ajouter la remote `heroku` à notre projet. 
Heroku est un service cloud qui permet de lancer des serveurs automatiquement en pushant son code sur les repositories de Heroku. Il s'utilise principalement avec Git et permet de tester facilement ses plateformes, voir de faire des environnements pour chaque développeurs, pour chaque branche et chaque personne qui souhaite tester une application web.


```bash
    git remote add heroku <url>
    heroku login
    git push heroku feature/heroku:master
```

La commande va rater, puisque pour le moment vous n'êtes pas à jour avec le travail d'Armande. 
Pour cela, vous pouvez faire un rebase avec `master` pour déplacer votre branche après le merge fait à l'étape 4.

```bash
    git rebase master
    git push heroku feature/heroku:master
```

Après quelques minutes vous pouvez alors ouvrir votre navigateur à l'URL qui s'affiche dans votre console.

## Etape 6

Retournez dans master et merger `feature/heroku` dans `master`.

## Etape 7 

Basculer sur `feature/dino` pour intégrer le travail de Dominique.
=======

## Etape 8

Tout est prêt pour fonctionner, il ne reste plus qu'à intégrer le code de Dominique. 

* Vous pouvez tout tester en local sur la branche de dominique à l'aide d'un rebase avant de merger
* Vous pouvez merger en local directement sur master et tester après.

Méthode avec le rebase:

```bash
    git checkout feature/dino
    git rebase master
```

Méthode avec le merge:

```bash
    git checkout master
    git merge feature/dino
```

Vous voilà prêt pour tester, vous pouvez faire au choix:

* `git push heroku feature/dino:master` ou `git push heroku master` selon que vous aillez fait un `rebase` ou un `merge` pour tester en ligne.
* `yarn start` pour tester en local.

## Etape 9 

1. Mergez dans `master` si ce n'est pas fait à l'étape 6.
2. Nous allons récupérer un commit qui nous intéresse et qui contient un message pour finir le TP : 

```bash
    git cherry-pick f1b63cc
```

Cette commande permet de récupérer un ou plusieurs commits de n'importe quelle branche et de les appliquer dans l'ordre à votre branche courante.
>>>>>>> feature/dino
